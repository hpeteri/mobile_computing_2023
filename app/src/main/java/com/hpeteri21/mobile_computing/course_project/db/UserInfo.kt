package com.hpeteri21.mobile_computing.course_project

import androidx.room.*
import java.util.*

@TypeConverters(StringListConverter::class)
@Entity(tableName = "users")
data class UserInfo(
  @ColumnInfo(name = "name")
  val name: String,
  
  @ColumnInfo(name = "password")
  val password: String,
  
  @ColumnInfo(name = "pin")
  val pin: Int,
  
  @ColumnInfo(name = "reminder_ids")
  var reminder_ids : MutableList<String>
){
  @PrimaryKey(autoGenerate = false)
  @ColumnInfo(name = "id")
  var id: String = UUID.randomUUID().toString()
}

object StringListConverter{
  @TypeConverter
  fun toString(StringList: List<String>?): String? {
    if (StringList == null) return null
    
    val stringList = mutableListOf<String>()
    StringList.forEach {
      if(it.isNotEmpty()) {
        stringList.add(it)
      }
    }
    
    return stringList.joinToString(",")
  }
  
  @TypeConverter
  fun toStringList(str: String?): List<String>? {
    if (str == null) return null
    
    val strings = mutableListOf<String>()
    
    val strList = str.split(",")
    for(str in strList){
      if(str.isNotEmpty()) {
        strings.add(str)
      }
    }
    
    return strings
  }
}
