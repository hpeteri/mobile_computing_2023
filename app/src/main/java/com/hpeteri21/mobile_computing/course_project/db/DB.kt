package com.hpeteri21.mobile_computing.course_project.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.hpeteri21.mobile_computing.course_project.Reminder
import com.hpeteri21.mobile_computing.course_project.UserInfo



@Database(entities = [UserInfo::class, Reminder::class], version = 1)
abstract class DB : RoomDatabase() {
  
  abstract fun getUserInfoDAO(): UserInfoDAO
  abstract fun getReminderDAO(): ReminderDAO
  
  companion object {
  
    @Volatile
    private var INSTANCE: DB? = null
  
    fun getDatabase(context: Context): DB {
      // if the INSTANCE is not null, then return it,
      // if it is, then create the database
      return INSTANCE ?: synchronized(this) {
        val instance = Room.databaseBuilder(
          context.applicationContext,
          DB::class.java,
          "mobile_computing_database.db"
        ).build()
        INSTANCE = instance
        // return instance
        instance
      }
    }
  }
}