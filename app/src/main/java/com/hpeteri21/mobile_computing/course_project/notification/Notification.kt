package com.hpeteri21.mobile_computing.course_project.notification

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.provider.Settings.System.getString
import android.util.Log
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Info
import androidx.compose.runtime.Composable
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.graphics.drawable.IconCompat
import androidx.work.*
import com.google.common.util.concurrent.ListenableFuture
import com.hpeteri21.mobile_computing.course_project.R
import com.hpeteri21.mobile_computing.course_project.Reminder
import java.io.DataInput
import java.util.Random
import java.util.UUID
import java.util.concurrent.TimeUnit

class SimpleNotification {
  
  companion object{

    @Volatile
    private var notification_manager : NotificationManager? = null
    
    @Volatile
    private var work_manager : WorkManager? = null
    
    fun getChannelID() : String{
      return "CHANNEL_ID_SIMPLE_NOTIFICATION"
    }
    
    fun createNotificationChannel(context : Context){
      // Create the NotificationChannel, but only on API 26+ because
      // the NotificationChannel class is new and not in the support library

      if(notification_manager != null){
        return
      }
      
      synchronized(this){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
          
          val channel_name = "channel_name"
          val description_text = ""
          val importance      = NotificationManager.IMPORTANCE_DEFAULT
          
          
          val channel = NotificationChannel(
            SimpleNotification.getChannelID(),
            channel_name,
            importance).apply {
              description = description_text}
            
          // Register the channel with the system
          notification_manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
          notification_manager!!.createNotificationChannel(channel)
          
          work_manager = WorkManager.getInstance(context)
          
        }
        
      }
    }
  
    fun scheduleNotificationFromReminder(delay : Long, reminder : Reminder) : UUID{
      
      val work_manager = work_manager!!
      val constraints = Constraints.Builder().build()
    
      val input : Data = Data.Builder()
        .putString("message", reminder.message)
        .build()
      
      val worker : WorkRequest = OneTimeWorkRequestBuilder<SimpleNotificationWorker>()
        .setInitialDelay(delay, TimeUnit.MILLISECONDS)
        .setConstraints(constraints)
        .setInputData(input)
        .build()
    
      
      work_manager.enqueue(worker)
      
      return worker.id
    }
    
    fun cancelNotificationWithID(id : String){
      try {
        val work_manager = work_manager!!
        
        work_manager.cancelWorkById(UUID.fromString(id))
        
      }catch(e : Exception){
        e.printStackTrace()
      }
    }
  }
}

class SimpleNotificationWorker(
  var context : Context,
  var params : WorkerParameters
) : Worker(context, params){
  
  override fun doWork(): Result {
    val id : Int = Random().nextInt()
    
    val builder = NotificationCompat.Builder(context,
                                             SimpleNotification.getChannelID())
      .setSmallIcon(
        androidx.constraintlayout.widget.R.drawable.abc_ic_star_black_48dp)
      .setContentTitle("Reminder Notification")
      .setContentText(params.inputData.getString("message"))
      .setPriority(NotificationCompat.PRIORITY_DEFAULT)
    
    with(NotificationManagerCompat.from(context)){
      notify(id, builder.build())
    }
    
    return Result.success()
  }
}
