package com.hpeteri21.mobile_computing.course_project.db

import android.app.Application
import androidx.lifecycle.*
import com.hpeteri21.mobile_computing.course_project.Reminder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ReminderRepository(private val dao : ReminderDAO) {
  suspend fun insert(item: Reminder) = dao.insert(item)
  suspend fun delete(item: Reminder) = dao.delete(item)
  suspend fun update(item: Reminder) = dao.update(item)
  fun allItems() = dao.getAll()
}

class ReminderViewModel(application : Application) : AndroidViewModel(application) {
  
  private val repo : ReminderRepository
  val data : LiveData<List<Reminder>>
  
  init{
    val dao : ReminderDAO = DB.getDatabase(application).getReminderDAO()
    repo = ReminderRepository(dao)
  
    data = dao.getAll()
  }
  
  fun add(item: Reminder){
    viewModelScope.launch(Dispatchers.IO){
      repo.insert(item)
    }
  }
  
  fun update(item: Reminder){
    viewModelScope.launch(Dispatchers.IO){
      repo.update(item)
    }
  }
  
  fun delete(item: Reminder){
    viewModelScope.launch(Dispatchers.IO){
      repo.delete(item)
    }
  }
}