package com.hpeteri21.mobile_computing.course_project.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.hpeteri21.mobile_computing.course_project.Reminder
import com.hpeteri21.mobile_computing.course_project.UserInfo

@Dao
interface ReminderDAO {
  @Insert(onConflict = OnConflictStrategy.REPLACE)
  suspend fun insert(item: Reminder)
  
  @Update
  suspend fun update(item: Reminder)
  
  @Delete
  suspend fun delete(item: Reminder)
  
  @Query("SELECT * FROM reminders")
  fun getAll(): LiveData<List<Reminder>>
}
