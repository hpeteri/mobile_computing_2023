package com.hpeteri21.mobile_computing.course_project.ui

import android.app.TimePickerDialog
import android.content.Context
import android.view.View
import android.view.Window
import android.widget.TimePicker
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.unit.dp
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalContext


@Composable
fun DoCommonSpacer(){
  Spacer(modifier = Modifier.height(10.dp))
}

@Composable
fun SetStatusBarColorToTheme(window : Window){
  window.statusBarColor = MaterialTheme.colors.background.toArgb()
  
  window.decorView.systemUiVisibility = if(isSystemInDarkTheme()){
    window.decorView.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
  }else{
    window.decorView.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
  }  
}

fun GetMainColumnModifier() : Modifier{
  val mod : Modifier = Modifier
      .fillMaxSize()
      .padding(20.dp)

  return mod
}

