package com.hpeteri21.mobile_computing.course_project

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "reminders")
data class Reminder(
  
  @ColumnInfo(name = "message")
  var message: String,
  
  @ColumnInfo(name = "location_x")
  var location_x: String,
  
  @ColumnInfo(name = "location_y")
  var location_y: String,
  
  @ColumnInfo(name = "reminder_time")
  var reminder_time: String,
  
  @ColumnInfo(name = "creation_time")
  var creation_time: String,
  
  @ColumnInfo(name = "user_id")
  var user_id: String,
  
  @ColumnInfo(name = "is_seen")
  var is_seen: Boolean,
  
  @ColumnInfo(name = "icon_id")
  var icon_id: Int

){
  
  @PrimaryKey(autoGenerate = false)
  @ColumnInfo(name = "id")
  var id: String = UUID.randomUUID().toString()
  
  @ColumnInfo(name = "notification_id")
  var notification_id : String = ""
}
