package com.hpeteri21.mobile_computing.course_project.db

import android.app.Application
import androidx.lifecycle.*
import androidx.lifecycle.viewmodel.compose.viewModel
import com.hpeteri21.mobile_computing.course_project.UserInfo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class UserInfoRepository(private val dao : UserInfoDAO) {
  suspend fun insert(item: UserInfo) = dao.insert(item)
  suspend fun update(item: UserInfo) = dao.update(item)
  suspend fun delete(item: UserInfo) = dao.delete(item)
  fun allItems() = dao.getAll()
}

class UserInfoViewModel(application : Application) : AndroidViewModel(application) {
  
  private val repo : UserInfoRepository
  val data : LiveData<List<UserInfo>>
  
  
  init{
    val dao : UserInfoDAO = DB.getDatabase(application).getUserInfoDAO()
    repo = UserInfoRepository(dao)
    
    data = dao.getAll()
  }
  
  fun add(item: UserInfo){
    viewModelScope.launch(Dispatchers.IO){
      repo.insert(item)
    }
  }
  
  fun update(item : UserInfo){
    viewModelScope.launch(Dispatchers.IO){
      repo.update(item)
    }
  }
  
  fun delete(item: UserInfo){
    viewModelScope.launch(Dispatchers.IO){
      repo.delete(item)
    }
  }
}
