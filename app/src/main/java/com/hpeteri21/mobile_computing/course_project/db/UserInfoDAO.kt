package com.hpeteri21.mobile_computing.course_project.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.hpeteri21.mobile_computing.course_project.UserInfo

@Dao
interface UserInfoDAO {
  @Insert(onConflict = OnConflictStrategy.REPLACE)
  suspend fun insert(item: UserInfo)
  
  @Delete
  suspend fun delete(item: UserInfo)
  
  @Update
  suspend fun update(item: UserInfo)
  
  @Query("SELECT * FROM users")
  fun getAll(): LiveData<List<UserInfo>>
  
  @Query("SELECT * FROM users WHERE name LIKE :name0")
  fun getUserWithName(name0 : String) : LiveData<List<UserInfo>>
}