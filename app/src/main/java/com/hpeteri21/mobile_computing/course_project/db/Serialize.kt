package com.hpeteri21.mobile_computing.course_project.db

import java.util.*

fun makeCurrentDate() : Date{
  return Calendar.getInstance().time;
}
fun makeDate(year : Int, month : Int, day : Int, hour : Int, minute : Int) : Date{
  val date = Date()
  
  date.date = day
  date.month = month - 1
  date.year = year - 1900
  date.hours = hour
  date.minutes = minute
  date.seconds = 0
  
  return date;
}

fun serializeDate(date : Date) : String{
  
  return date.toString();
}

fun deserializeDate(string : String) : Date{
  
  try{
    var date : Date = Date(string);
    return date;
  }catch(e : Exception){
    //pass
  }
  
  return Calendar.getInstance().time;
}
