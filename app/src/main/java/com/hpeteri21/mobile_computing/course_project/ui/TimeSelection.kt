import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.graphics.drawable.VectorDrawable
import android.widget.TimePicker
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.material.icons.filled.Info
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.semantics.Role
import com.hpeteri21.mobile_computing.course_project.ui.dateStringParser
import com.hpeteri21.mobile_computing.course_project.ui.getCurrentDateString
import java.lang.Integer.max
import java.util.*
import kotlin.math.min

fun getTimeStringFrom(date_string : String) : String {
  var date : Date = Calendar.getInstance().time
  
  try {
    date = Date(date_string)
  
  }catch(e: Exception){
  
  }
  
  val hours = date.hours
  val minutes = date.minutes
  
  var hour_str = "$hours"
  var minute_str = "$minutes"
  
  if(hour_str.length == 1){
    hour_str = "0$hour_str"
  }
  
  if(minute_str.length == 1){
    minute_str = "0$minute_str"
  }
  
  return "$hour_str:$minute_str"
}

fun getCurrentTimeString() : String{
  val date = Calendar.getInstance().time
  
  return getTimeStringFrom(date.toString())
}

fun isValidTimeString(date : String) : Boolean {
  try{
    
    val breakdown = date.split(":")
    
    if(breakdown.size != 2){
      return false
    }
    
    var hours = breakdown[0].toInt()
    var minutes = breakdown[1].toInt()
    
    if(!(hours >= 0 && hours <= 23 && minutes >= 0 && minutes <= 59)){
      return false
    }
    
    return true
  }catch (e: Exception) {
    //failed parse, returns false anyways
  }
  return false
}

fun parseStringTime(time : String) : Array<Int>{
  var hour : Int = 0
  var minute : Int = 0
  
  try{
    var split = time.split(":")
    if(split.size == 2){
      hour = split[0].toInt()
      minute = split[1].toInt()
      
      hour = min(hour, 23)
      hour = max(hour, 0)
      
      minute = min(minute, 59)
      minute = max(minute, 0)
    }
  }catch(e : Exception){
  
  }
  return arrayOf( hour, minute )
}

@Composable
fun DoTimeSelection(
  label: String,
  selection: String = "",
  onSelectionChanged: TimePickerDialog.OnTimeSetListener = TimePickerDialog.OnTimeSetListener{ time_picker : TimePicker, hour : Int, minute : Int -> },
  onTextChanged: (String) -> Unit = {},
  modifier : Modifier = Modifier.fillMaxWidth(),
  ){
  val dialog = TimePickerDialog(LocalContext.current,
    onSelectionChanged,
  0,
    0,
    true
    )
  
  OutlinedTextField(
    value = selection,
    onValueChange = onTextChanged,
    label = { Text( label ) },
    leadingIcon = {
      Icon(
        Icons.Default.Info,
        contentDescription = null,
        modifier = Modifier
          .clickable(
            enabled = true,
            role = Role.Button,
            onClick = {dialog.show()}
          )
      )
    },
    readOnly = false,
    modifier = modifier)
}
