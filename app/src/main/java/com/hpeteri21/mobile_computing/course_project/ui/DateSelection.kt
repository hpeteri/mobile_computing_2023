package com.hpeteri21.mobile_computing.course_project.ui

import android.app.DatePickerDialog
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.semantics.Role
import java.util.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.unit.dp
import androidx.compose.ui.Modifier
import com.hpeteri21.mobile_computing.course_project.db.makeDate
import java.text.SimpleDateFormat

fun isValidDateString(date : String) : Boolean {
  try{

    val breakdown = date.split("/")

    if(breakdown.size != 3){
      return false
    }
    
    breakdown[0].toInt()
    breakdown[1].toInt() - 1
    breakdown[2].toInt()
    
    return true
  }catch (e: java.lang.Exception) {
    //failed parse, returns false anyways
  }
  return false
}

fun getDateStringFrom(date_string : String) : String {
  try {
    val date = Date(date_string)
  
    val year = date.year + 1900
    val month = date.month + 1
    val day = date.date
  
    return "$day/$month/$year"
  }catch(e: Exception){
  
  }
  return getCurrentDateString()
}

fun getCurrentDateString() : String{
  val date = dateStringParser(calendar = Calendar.getInstance(), date = "")
  
  val year = date[0]
  val month = date[1] + 1
  val day = date[2]
  
  return "$day/$month/$year"
}

fun dateStringParser( calendar: Calendar, date: String ): Array<Int> {
  var day = calendar[Calendar.DAY_OF_MONTH]
  var month = calendar[Calendar.MONTH]
  var year = calendar[Calendar.YEAR]
  
  if(date.isNotEmpty() && date.isNotBlank() && date.contains("/")) {
  	val breakdown = date.split( "/" )
  	  
  	if(breakdown.count() == 3 &&
  	  breakdown[0].isNotEmpty() && breakdown[0].isNotBlank() &&
  	  breakdown[1].isNotEmpty() && breakdown[1].isNotBlank() &&
  	  breakdown[2].isNotEmpty() && breakdown[2].isNotBlank()
  	){
  	  try {
  	    day = breakdown[0].toInt()
  	    month = breakdown[1].toInt() - 1
  	    year = breakdown[2].toInt()
  	  }catch(e : java.lang.Exception){
  	    
  	  }
    }
  }

  return arrayOf( year, month, day )
}

/*
fun dateStringConverter( date: String ): Long {
    val calendar = Calendar.getInstance()
    
    if( date.isNotEmpty() && date.isNotBlank() && date.contains( "/" ) ) {
      val breakdown = date.split( "/" )
      
      if( breakdown.count() == 3 &&
      breakdown[0].isNotEmpty() && breakdown[0].isNotBlank() &&
      breakdown[1].isNotEmpty() && breakdown[1].isNotBlank() &&
      breakdown[2].isNotEmpty() && breakdown[2].isNotBlank()
    ) {
      calendar[Calendar.DAY_OF_MONTH] = breakdown[0].toInt()
      calendar[Calendar.MONTH] = breakdown[1].toInt() - 1
      calendar[Calendar.YEAR] = breakdown[2].toInt()
    }
  }
  return calendar.timeInMillis
}

*/

@Composable
fun DoDateSelection(
  label: String,
  selection: String = "",
  onSelectionChanged: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener{ datePicker, year, month, day ->  },
  onTextChanged: (String ) -> Unit = {},
  modifier: Modifier = Modifier.fillMaxWidth()
){
  val calendar = Calendar.getInstance()
  val pickedDate = dateStringParser( calendar = calendar, date = selection )
  
  calendar.timeZone
  val dialog = DatePickerDialog(
    LocalContext.current,
    onSelectionChanged,
    pickedDate[0],
    pickedDate[1],
    pickedDate[2]
  )
  
  OutlinedTextField(
    value = selection,
    onValueChange = onTextChanged,
    label = { Text( label ) },
    leadingIcon = {
      Icon(
        Icons.Default.DateRange,
        contentDescription = null,
        modifier = Modifier
            .clickable(
              enabled = true,
              role = Role.Button,
              onClick = {dialog.show()}
            )
          )
        },
    readOnly = false,
    modifier = modifier)
}



  
