package com.hpeteri21.mobile_computing.course_project.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.Colors
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color


val pink_0 = Color(0xffffaeb9)
val pink_1 = Color(0xffff4366)

val blue_0 = Color(0xff64a4fb)
val blue_1 = Color(0xffa57fd0)
val blue_2 = Color(0xff4b5075)

val orange_0 = Color(0xffffa500)

val black_0  = Color(0xff111320)

val gray_0 = Color(0xff454545)
val gray_1 = Color(0xff757575)
val gray_2 = Color(0xffaaaaaa)
val gray_3 = Color(0xffeeeeee)
val gray_4 = Color(0xfff3f3f3)
/*
Colors(
  primary: Color,
  primaryVariant: Color,
  secondary: Color,
  secondaryVariant: Color,
  background: Color,
  surface: Color,
  error: Color,
  onPrimary: Color,
  onSecondary: Color,
  onBackground: Color,
  onSurface: Color,y
  onError: Color,
  isLight: Boolean)
*/

private val DarkColorPalette = darkColors(
  primary          = blue_1,
  primaryVariant   = blue_2,
  secondary        = pink_0,
  secondaryVariant = pink_1,
  background       = black_0,
  surface          = gray_0,
  //error            = Color,
  //onPrimary        = Color,
  //onSecondary      = Color,
  //onBackground     = Color,
  //onSurface        = Color,
  //onError          = Color,
)

private val LightColorPalette = lightColors(
  primary          = blue_0,
  primaryVariant   = blue_1,
  secondary        = pink_0,
  secondaryVariant = pink_1,
  background       = gray_4,
  surface          = gray_3,
  //error            = Color,
  //onPrimary        = Color,
  //onSecondary      = Color,
  //onBackground     = Color,
  //onSurface        = Color,
  //onError          = Color,
)

@Composable
fun Course_projectTheme(
  isDarkTheme: Boolean = isSystemInDarkTheme(),
  content: @Composable () -> Unit
){

  val colors = if(isDarkTheme){
    DarkColorPalette
  }else{
    LightColorPalette
  }
  
  MaterialTheme(
    colors     = colors,
    typography = Typography,
    shapes     = Shapes,
    content    = content,
  )
}
