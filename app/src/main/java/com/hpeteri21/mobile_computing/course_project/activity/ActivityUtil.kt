package com.hpeteri21.mobile_computing.course_project.activity

import android.content.Intent

fun SetCommonIntentState(intent: Intent) : Intent{
  intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
  intent.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
  
  return intent
}
