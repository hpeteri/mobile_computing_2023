package com.hpeteri21.mobile_computing.course_project.activity

import android.content.Intent
import android.os.Bundle
import android.view.Window
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.focusable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.hpeteri21.mobile_computing.course_project.*
import com.hpeteri21.mobile_computing.course_project.db.DB
import com.hpeteri21.mobile_computing.course_project.db.UserInfoViewModel
import com.hpeteri21.mobile_computing.course_project.notification.SimpleFaderNotification
import com.hpeteri21.mobile_computing.course_project.notification.SimpleNotification
import com.hpeteri21.mobile_computing.course_project.notification.SimpleNotification.Companion.createNotificationChannel
import com.hpeteri21.mobile_computing.course_project.ui.DoCommonSpacer
import com.hpeteri21.mobile_computing.course_project.ui.GetMainColumnModifier
import com.hpeteri21.mobile_computing.course_project.ui.SetStatusBarColorToTheme
import com.hpeteri21.mobile_computing.course_project.ui.theme.Course_projectTheme

class LoginActivity : ComponentActivity() {
  
  lateinit var user_info_view: UserInfoViewModel;
  
  fun createDefaultLoginInfo() {
  
    val db = DB.getDatabase(this)
    user_info_view = ViewModelProvider(this).get(UserInfoViewModel::class.java)
    user_info_view.data.observe(this, Observer{
      users -> run {
        if(users.size == 0){
          user_info_view.add(
            UserInfo(name = "hpeteri21",
            password = "salasana123",
            pin = 123456,
          reminder_ids = mutableListOf<String>())
          )
        }
    }
    })
  }
  
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    createDefaultLoginInfo()
    createNotificationChannel(this)
    SimpleFaderNotification.init(this)
    
    setContent{
      Course_projectTheme{
        Surface(color = MaterialTheme.colors.background,
                modifier = Modifier.fillMaxWidth()
        ){
          DoLoginMethodNavigation(loginCallback = {
            setActivityToMessages()
                                                  },
          window = window,
          )
        }
      }
    }
  }

  //callback for login button
  fun setActivityToMessages(){
    var intent = Intent(this, MessagesActivity::class.java)
    intent = SetCommonIntentState(intent)
    startActivity(intent)
  }
  
  fun removeWhitespace(text : String) : String{
    return text.replace("\\s".toRegex(), "")
  }
  
  fun removeNonDigit(text : String) : String{
    return text.replace("[^0-9]".toRegex(), "")
  }
  
  fun authenticate(
    username : String,
    password : String?,
    pin : Int,
    doLoginCallback : () -> Unit
  ){
    user_info_view.data.observe(this, Observer {
      users -> run {
  
      for(user in users){
          if(user.name == username){
            if(password.isNullOrBlank()) {
              if(user.pin == pin){
                ApplicationState.getInstance().logged_in_user = user
                doLoginCallback()
                return@run
              }
            }else {
              if (user.password == password) {
                ApplicationState.getInstance().logged_in_user = user
                doLoginCallback()
                return@run
              }
            }
          }
        }
      if(ApplicationState.getInstance().logged_in_user == null){
        SimpleFaderNotification.DoFaderNotification("Authentication failed")
      }
    }
    })
  }
  
  @Composable
  fun DoLoginMethodNavigation(
    loginCallback : () -> Unit,
    window : Window,
    nav_state: ApplicationNavigationState = rememberNavigationState()
  ) {
    NavHost(
      navController = nav_state.controller,
      startDestination = "login_password"
    ) {
      composable(route = "login_password") {
        DoNormalLoginScreen(loginCallback,
          window,
          nav_state)
      }
      composable(route = "login_pin") {
        DoPinLoginScreen(loginCallback,
          window,
          nav_state)
      }
    }
  }
  
  @OptIn(ExperimentalComposeUiApi::class)
  @Composable
  fun DoPinLoginScreen(
    loginCallback : () -> Unit,
    window : Window,
    nav_state: ApplicationNavigationState
  ) {
    SetStatusBarColorToTheme(window)
    
    val keyboard_controller = LocalSoftwareKeyboardController.current
    
    val focus_done = remember { FocusRequester() }
    
    val username = remember { mutableStateOf("") }
    val pin = remember { mutableStateOf("") }
    
    Column(
      modifier = GetMainColumnModifier(),
      horizontalAlignment = Alignment.CenterHorizontally,
    ) {

      Icon(
        imageVector = Icons.Default.AccountCircle,
        contentDescription = null,
        modifier = Modifier.fillMaxSize(0.25f)
      )
      
      Text(text = "Log in")
          
      OutlinedTextField(
        modifier = Modifier.fillMaxWidth(),
        label = { Text("username") },
        value = username.value,
        maxLines = 1,
        onValueChange = { text -> username.value = removeWhitespace(text) },
        keyboardOptions = KeyboardOptions(
          capitalization = KeyboardCapitalization.None,
          autoCorrect = false,
          keyboardType = KeyboardType.Text,
          imeAction = ImeAction.Next,
        ),
      )
      
      DoCommonSpacer()
      OutlinedTextField(
        modifier = Modifier.fillMaxWidth(),
        label = { Text("pin") },
        value = pin.value,
        maxLines = 1,
        onValueChange = { text -> pin.value = removeNonDigit(text) },
        keyboardOptions = KeyboardOptions(
          capitalization = KeyboardCapitalization.None,
          autoCorrect = false,
          keyboardType = KeyboardType.NumberPassword,
          imeAction = ImeAction.Done
        ),
        keyboardActions = KeyboardActions(
          onDone = {
            keyboard_controller?.hide()
            focus_done.requestFocus()
          }
        ),
        visualTransformation = PasswordVisualTransformation(),
      )
      
      DoCommonSpacer()
      DoCommonSpacer()
      
      Button(
        modifier = Modifier
          .fillMaxWidth(0.5f)
          .focusRequester(focus_done)
          .focusable(),
        onClick = {
          var pinint = -1
          try{
            pinint = pin.value.toInt()
          }catch(e : Exception){
          }
          
          authenticate(username.value,
            null,
            pinint,
            loginCallback)
          username.value = ""
          pin.value = ""
        }
      ) {
        Text(text = "Log in")
      }
      
      Button(
        modifier = Modifier
          .fillMaxWidth(0.5f),
        onClick = {
          nav_state.navigateToAndClearHistory("login_password")
          
        }
      ) {
        Text(text = "Login with password")
      }
    }
  }
  
  @OptIn(ExperimentalComposeUiApi::class)
  @Composable
  fun DoNormalLoginScreen(
    loginCallback : () -> Unit,
    window : Window,
    nav_state: ApplicationNavigationState
  ) {
    
    SetStatusBarColorToTheme(window)
    
    val keyboard_controller = LocalSoftwareKeyboardController.current
    val focus_done = remember { FocusRequester() }
    
    val username = remember { mutableStateOf("") }
    val password = remember { mutableStateOf("") }
    
    Column(
      modifier = GetMainColumnModifier(),
      horizontalAlignment = Alignment.CenterHorizontally,
    ) {
      
      Icon(
        imageVector = Icons.Default.AccountCircle,
        contentDescription = null,
        modifier = Modifier.fillMaxSize(0.25f)
      )
      
      Text(text = "Log in")
       
      OutlinedTextField(
        modifier = Modifier.fillMaxWidth(),
        label = { Text("username") },
        value = username.value,
        maxLines = 1,
        onValueChange = { text -> username.value = removeWhitespace(text) },
        keyboardOptions = KeyboardOptions(
          capitalization = KeyboardCapitalization.None,
          autoCorrect = false,
          keyboardType = KeyboardType.Text,
          imeAction = ImeAction.Next,
        ),
      )
      
      DoCommonSpacer()
      
      OutlinedTextField(
        modifier = Modifier.fillMaxWidth(),
        label = { Text("password") },
        value = password.value,
        maxLines = 1,
        onValueChange = { text -> password.value = removeWhitespace(text) },
        keyboardOptions = KeyboardOptions(
          capitalization = KeyboardCapitalization.None,
          autoCorrect = false,
          keyboardType = KeyboardType.Password,
          imeAction = ImeAction.Done
        ),
        keyboardActions = KeyboardActions(
          onDone = {
            keyboard_controller?.hide()
            focus_done.requestFocus()
          }
        ),
        visualTransformation = PasswordVisualTransformation(),
      )
      
      DoCommonSpacer()
      DoCommonSpacer()
      
      Button(
        modifier = Modifier
          .fillMaxWidth(0.5f)
          .focusRequester(focus_done)
          .focusable(),
        onClick = {
          authenticate(username.value,
                        password.value,
                        0,
                        loginCallback)
          username.value = ""
          password.value = ""
        }
      ) {
        Text(text = "Log in")
      }
      
      Button(
        modifier = Modifier
          .fillMaxWidth(0.5f),
        onClick = {
          nav_state.navigateToAndClearHistory("login_pin")
        }
      ) {
        Text(text = "Login with pin")
      }
    }
  }
}

