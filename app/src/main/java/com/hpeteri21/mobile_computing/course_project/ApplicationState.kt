package com.hpeteri21.mobile_computing.course_project

import android.app.Application
import android.content.Context
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import androidx.room.Room
import com.hpeteri21.mobile_computing.course_project.db.DB

class ApplicationState(){
  
  var logged_in_user : UserInfo? = null;
  
  companion object {
    
    @Volatile
    private var INSTANCE: ApplicationState? = null
    
    fun getInstance(): ApplicationState {
      
      return INSTANCE ?: synchronized(this) {
        
        val instance = ApplicationState()
        INSTANCE = instance
        instance
      }
    }
  }
}

class ApplicationNavigationState(
  val controller: NavHostController,
){
  
  fun navigateBack(){
    controller.popBackStack()
  }
  
  fun clearHistory(){
    controller.backQueue.clear()
  }
  
  fun navigateToAndClearHistory(dest : String){
    clearHistory()
    controller.navigate(dest)
  }
  
  fun navigateTo(dest: String){
    
    controller.navigate(dest)
  }
}

@Composable
fun rememberNavigationState(
  navController: NavHostController = rememberNavController()
) = remember(navController) {
  ApplicationNavigationState(navController)
}
