package com.hpeteri21.mobile_computing.course_project.activity

import DoTimeSelection
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.graphics.Paint.Align
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.dp
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.hpeteri21.mobile_computing.course_project.*
import com.hpeteri21.mobile_computing.course_project.db.*
import com.hpeteri21.mobile_computing.course_project.notification.SimpleFaderNotification
import com.hpeteri21.mobile_computing.course_project.notification.SimpleNotification
import com.hpeteri21.mobile_computing.course_project.ui.*
import com.hpeteri21.mobile_computing.course_project.ui.theme.Course_projectTheme
import getCurrentTimeString
import getTimeStringFrom
import isValidTimeString
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.Period
import java.time.format.DateTimeFormatter
import java.util.*


class MessagesActivity : ComponentActivity() {
  
  lateinit var reminder_view: ReminderViewModel
  lateinit var user_info_view: UserInfoViewModel
  var message_to_edit : Reminder? = null
  
  companion object{
    var icons : Array<ImageVector> = arrayOf(
      
      Icons.Default.Star,
      Icons.Default.Email,
      Icons.Default.ShoppingCart,
      Icons.Default.AddCircle,
      Icons.Default.CheckCircle,
    )
  }
    
  // callback for logout button
  private fun setActivityToLogin() {
    var intent = Intent(this, LoginActivity::class.java)
    intent = SetCommonIntentState(intent);
    ApplicationState.getInstance().logged_in_user = null
    startActivity(intent)
  }
  
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    
    SimpleFaderNotification.init(this)
    val db = DB.getDatabase(this)
    reminder_view = ViewModelProvider(this).get(ReminderViewModel::class.java)
    user_info_view = ViewModelProvider(this).get(UserInfoViewModel::class.java)
    
    setContent {
      Course_projectTheme {
        Surface(
          color = MaterialTheme.colors.background,
          modifier = Modifier.fillMaxWidth()
        ) {
          
          DoMessageActivityNavigation(loginCallback = { setActivityToLogin() })
          
        }
      }
    }
  }
  
  @Composable
  fun DoMessageActivityNavigation(
    loginCallback: () -> Unit,
    nav_state: ApplicationNavigationState = rememberNavigationState()
  ) {
    NavHost(
      navController = nav_state.controller,
      startDestination = "messages_view"
    ) {
      
      composable(route = "messages_view") {
        DoMessagesScreen(
          logoutCallback = {
            nav_state.clearHistory()
            loginCallback()
          },
          window = window,
          nav_state = nav_state
        )
      }
      
      composable(route = "messages_create") {
        DoCreateOrEditMessageView(nav_state = nav_state, null);
      }
      
      composable(route = "messages_edit"){
        DoCreateOrEditMessageView(nav_state = nav_state, message_to_edit);
  
      }
    }
  }
  
  @Composable
  fun DoMessagesScreen(
    nav_state: ApplicationNavigationState,
    logoutCallback: () -> Unit,
    window: Window
  ) {
    
    SetStatusBarColorToTheme(window)
    
    Column(
      modifier = GetMainColumnModifier(),
      horizontalAlignment = Alignment.CenterHorizontally,
      verticalArrangement = Arrangement.Center
    ){
      Column(
        modifier = Modifier
          .fillMaxWidth()
          .height(500.dp)
          .border(BorderStroke(2.dp, MaterialTheme.colors.primaryVariant))
      ) {
        DoReminderMessages(nav_state)
      }
      
      DoCommonSpacer()
      
      Button(
        onClick = logoutCallback
      ) {
        Text(text = "Logout")
      }
      
      DoCommonSpacer()
    }
    
    Row(
      modifier = Modifier
        .fillMaxSize()
        .padding(50.dp),
      horizontalArrangement = Arrangement.End,
      verticalAlignment = Alignment.Bottom,
    ) {
      FloatingActionButton(
        onClick = {
          nav_state.navigateTo("messages_create")
          
        }) {
        Icon(Icons.Filled.Add, "")
      }
    }
  }
  
  // Display time info on card
  fun getReminderTimeInfo(reminder : Reminder) : String{
    val fmt = SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault())
    val date : Date = Date(reminder.reminder_time)
    
    val str : String = fmt.format(date)
    return str
  }
  
  @Composable
  fun DoSingleReminderMessageCard(
    reminder : Reminder,
    nav_state: ApplicationNavigationState,
  ){
    val pad = 3.dp
    
    Row(
      Modifier
        .padding(pad)
    ) {
      Card(
        elevation = 2.dp,
        modifier = Modifier
          .padding(pad)
          .fillMaxWidth()
          .height(75.dp)
      ) {
        
        Row(modifier = Modifier.fillMaxSize()) {
        
          Row(
            modifier = Modifier.fillMaxWidth(0.85f),
            horizontalArrangement = Arrangement.Start) {
  
            // Reminder icon
            Icon(
              modifier = Modifier.padding(pad),
              imageVector = icons[reminder.icon_id],
              contentDescription = null,
            )
  
            // Text fields
            Column(modifier = Modifier
              .fillMaxWidth()
              .padding(2.dp)) {
              
              Text(
                modifier = Modifier.padding(pad),
                text = getReminderTimeInfo(reminder)
              )
              
              Text(
                modifier = Modifier.padding(pad),
                text = reminder.message
              )
            }
          }
          
          Row(
            modifier = Modifier
              .fillMaxWidth(1.0f)
              .fillMaxHeight(1.0f),
            horizontalArrangement = Arrangement.End) {
  
            //Buttons
            Column() {
              
              //Delete
              Button(
                modifier = Modifier
                  .fillMaxHeight(0.5f)
                  .padding(pad),
                onClick = {
                  
                    try{
                      val user_info = ApplicationState.getInstance().logged_in_user!!;
            
                      SimpleNotification.cancelNotificationWithID(reminder.notification_id)
                      user_info.reminder_ids.remove(reminder.id)
      
                      user_info_view.update(user_info)
                      reminder_view.delete(reminder)
                      
                      SimpleFaderNotification.DoFaderNotification("Reminder removed")
            
                    }catch(e : Exception){
      
                    }
                }) {
                Icon(
                  Icons.Default.Delete,
                  contentDescription = null
                )
              }
              
              //Edit
              Button(
                modifier = Modifier
                  .fillMaxHeight(1.0f)
                  .padding(pad),
                onClick = {
                  message_to_edit = reminder
                  nav_state.navigateTo("messages_edit")
                }) {
                Icon(
                  Icons.Default.Edit,
                  contentDescription = null
                )
              }
            }
          }
        }
      }
    }
  }
  
  @Composable
  fun DoReminderMessages(
    nav_state: ApplicationNavigationState) {
    
    var show_all = remember{ mutableStateOf(true )}
    var messages = remember {
      mutableStateListOf<Reminder>()
    }
    val now = Calendar.getInstance().time
    val user_info = ApplicationState.getInstance().logged_in_user ?: return;
    reminder_view.data.observe(this, Observer { reminders ->
      run {
        messages.clear()
  
        for (reminder in reminders) {
          if (reminder.user_id == user_info.id) {
            
            if(Date(reminder.reminder_time).time - now.time < 0 || show_all.value) {
              messages.add(reminder)
            }
          }
        }
      }
    })
    
    Row(verticalAlignment = Alignment.CenterVertically) {
      Checkbox(show_all.value,
        onCheckedChange = { show_all.value = !show_all.value })
      Text("show all")
    }
    LazyColumn(
    ) {
      items(items = messages)
      { message ->
        DoSingleReminderMessageCard(message, nav_state)
      }
    }
  }
  
  @Composable
  fun DoCreateOrEditMessageView(
    nav_state: ApplicationNavigationState,
    reminder_to_edit : Reminder?
    ) {
    
    val message_text = remember {
      if(reminder_to_edit == null){
        mutableStateOf("")
      }else{
        mutableStateOf(reminder_to_edit.message)
      }
    }
  
    val location_x_text = remember {
      if(reminder_to_edit == null){
        mutableStateOf("0")
      }else{
        mutableStateOf(reminder_to_edit.location_x)
      }
    }
    
    val location_y_text = remember {
      if(reminder_to_edit == null){
        mutableStateOf("0")
      }else{
        mutableStateOf(reminder_to_edit.location_y)
      }
    }
    
    val date_text = remember {
        if(reminder_to_edit == null){
          mutableStateOf(getCurrentDateString())
        }else{
          mutableStateOf(getDateStringFrom(reminder_to_edit.reminder_time))
        }
    }
    
    val time_text = remember {
      if(reminder_to_edit == null){
        mutableStateOf(getCurrentTimeString())
      }else{
        mutableStateOf(getTimeStringFrom(reminder_to_edit.reminder_time))
      }
    }
    
  
    val icon_id = remember {
      if(reminder_to_edit == null){
        mutableStateOf(0)
      }else{
        mutableStateOf(reminder_to_edit.icon_id)
      }
    }
  
    val create_notification = remember{ mutableStateOf(true)}
  
    //backbutton
    Column(
      modifier = GetMainColumnModifier(),
      horizontalAlignment = Alignment.Start,
      verticalArrangement = Arrangement.Top
    ){
      Button(onClick = {nav_state.navigateBack()}){
        Icon(
          Icons.Default.ArrowBack,
          contentDescription = null)
      }
    }
    
    // reminder fields
    Column(
      modifier = GetMainColumnModifier(),
      horizontalAlignment = Alignment.CenterHorizontally,
      verticalArrangement = Arrangement.Center
    ) {
      if(reminder_to_edit == null) {
        Text("Create new reminder")
      }else{
        Text("Edit reminder")
      }
      DoCommonSpacer()
      OutlinedTextField(
        value = message_text.value,
        label = {Text("message")},
        onValueChange = {
          message_text.value = it
        },
        leadingIcon = {
          Icon(
            Icons.Default.Edit,
            contentDescription = null,
          )},
        modifier = Modifier.fillMaxWidth()
      )
      
      DoCommonSpacer()
  
      
      DoDateSelection(
        label = "date",
        selection = date_text.value,
        onTextChanged = {
            date_text.value = it;
        },
        onSelectionChanged = DatePickerDialog.OnDateSetListener{
            datePicker, year : Int, month : Int, day : Int -> run {
              val month2 = month + 1
              date_text.value = "$day/$month2/$year"
        }}
      )
      
      DoCommonSpacer()
      
      DoTimeSelection(
        "time",
        selection = time_text.value,
        onTextChanged = {
          time_text.value = it
        },
        onSelectionChanged = TimePickerDialog.OnTimeSetListener{
          timePicker, hour : Int, minute : Int -> run{
          
          var hour_string = "$hour"
          if(hour_string.length == 1){
            hour_string = "0$hour_string"
          }
          
          var minute_string = "$minute"
          if(minute_string.length == 1){
            minute_string = "0$minute_string"
          }
         
          time_text.value = "$hour_string:$minute_string"
          }
        }
      )
      
      DoCommonSpacer()
      
      OutlinedTextField(
        value = location_x_text.value,
        label = {Text("location_x")},
        onValueChange = {
        },
        modifier = Modifier.fillMaxWidth()
      )
     
      
      DoCommonSpacer()
     
      OutlinedTextField(
        value = location_x_text.value,
        label = {Text("location_y")},
        onValueChange = {
        },
        modifier = Modifier.fillMaxWidth()
      )
  
      DoCommonSpacer()
      
      Row(
        verticalAlignment = Alignment.CenterVertically,
      horizontalArrangement = Arrangement.Start){
        Text(
          modifier = Modifier.padding(2.dp),
          text = "Icon:")
        
        Button(onClick = {icon_id.value = (icon_id.value + 1) % icons.size}) {
          Icon(
          imageVector = icons[icon_id.value],
          contentDescription = null,
        )
        }
      }

      DoCommonSpacer()
  
      //create notification or not
      Row(verticalAlignment = Alignment.CenterVertically){
        
        Checkbox(create_notification.value, onCheckedChange = {create_notification.value = !create_notification.value})
        Text("Create notification")
        
      }
      
      Button(onClick = {
        try{
            val user_info = ApplicationState.getInstance().logged_in_user;
          
            if(isValidDateString(date_text.value) && isValidTimeString(time_text.value)){
              
              var date_str = date_text.value.split("/")
              var time_str = time_text.value.split(":")
              
              var year : Int = date_str[2].toInt()
              var month : Int = date_str[1].toInt()
              var day : Int = date_str[0].toInt()
              
              var hours : Int = time_str[0].toInt()
              var minute : Int = time_str[1].toInt()
              
              var reminder_date = makeDate(year, month, day, hours, minute)
  
              
              if(reminder_to_edit == null) {
                val new_reminder = Reminder(
                  message = message_text.value,
                  location_x = location_x_text.value,
                  location_y = location_y_text.value,
                  reminder_time = serializeDate(reminder_date),
                  creation_time = serializeDate(makeCurrentDate()),
                  user_id = user_info!!.id,
                  is_seen = false,
                  icon_id = icon_id.value,
                )
  
  
                //create notification
                if(create_notification.value) {
                  val now: Date = Calendar.getInstance().time
                  var delay: Long = Date(new_reminder.reminder_time).time - now.time
                  new_reminder.notification_id =
                    SimpleNotification.scheduleNotificationFromReminder(delay, new_reminder)
                      .toString()
                }else{
                  new_reminder.notification_id = ""
                }
                
                user_info.reminder_ids.add(new_reminder.id)
  
                user_info_view.update(user_info)
                reminder_view.add(new_reminder)
  
                SimpleFaderNotification.DoFaderNotification("Reminder created")
                nav_state.navigateTo("messages_view")
              }else{
                //edit existing reminder
                var reminder = reminder_to_edit!!
                
                reminder.message = message_text.value
                reminder.location_x = location_x_text.value
                reminder.location_y = location_y_text.value
                reminder.reminder_time = serializeDate(reminder_date)
                reminder.user_id = user_info!!.id
                reminder.is_seen = false
                reminder.icon_id = icon_id.value
  
                //cancel old notification
                SimpleNotification.cancelNotificationWithID(reminder.notification_id)
  
                //create notification
                if(create_notification.value) {
                  val now: Date = Calendar.getInstance().time
                  var delay: Long = Date(reminder.reminder_time).time - now.time
                  reminder.notification_id =
                    SimpleNotification.scheduleNotificationFromReminder(delay, reminder).toString()
                }else{
                  reminder.notification_id = ""
                }
                
                reminder_view.update(reminder)
                SimpleFaderNotification.DoFaderNotification("Reminder edited")
                nav_state.navigateTo("messages_view")
              }
            }
        }catch(e : Exception){
        
        }
        
      }){
        if(reminder_to_edit == null) {
          Text("Create");
        }else {
          Text("Update");
        }
      }
      DoCommonSpacer()
    }
  }
}