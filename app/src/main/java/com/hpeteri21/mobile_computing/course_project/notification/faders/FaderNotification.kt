package com.hpeteri21.mobile_computing.course_project.notification

import android.annotation.SuppressLint
import android.content.Context
import android.widget.Toast
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext


class SimpleFaderNotification{
  companion object{
    
    @SuppressLint("StaticFieldLeak")
    lateinit var context : Context;
    
    fun init(context : Context){
      this.context = context
    }
    
    fun DoFaderNotification(prompt : String){
      try {
        val toast = Toast.makeText(context, prompt, Toast.LENGTH_SHORT)
        toast.show()
      }catch(e: Exception){
        e.printStackTrace()
      }
    }
    
  }
}
